/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2015 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2015 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.bitcoin.blockchaininfo.json;

import java.io.IOException;
import java.util.Map;

import org.savapage.core.json.JsonAbstractBase;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class ExchangeRate extends JsonAbstractBase {

    /**
     * 15 minutes delayed market price.
     */
    @JsonProperty("15m")
    private double delayed;

    /**
     * Most recent market price.
     */
    private double last;

    /**
     * Current buy price.
     */
    private double buy;

    /**
     * Current sell price
     */
    private double sell;

    /**
     * Currency symbol
     */
    private String symbol;

    public double getDelayed() {
        return delayed;
    }

    public void setDelayed(double delayed) {
        this.delayed = delayed;
    }

    public double getLast() {
        return last;
    }

    public void setLast(double last) {
        this.last = last;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Creates a {@link Map} of {@link ExchangeRate} instances with Currency
     * Code key (e.g. "EUR", "USD").
     *
     * @param json
     *            The JSON string.
     * @return The {@link Map}.
     */
    public static Map<String, ExchangeRate>
            createExchangeRates(final String json) throws IOException {
        return getMapper().readValue(json,
                new TypeReference<Map<String, ExchangeRate>>() {
                });
    }

}
