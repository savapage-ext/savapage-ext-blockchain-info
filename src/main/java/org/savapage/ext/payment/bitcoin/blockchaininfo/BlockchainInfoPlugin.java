/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2015 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2015 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.bitcoin.blockchaininfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.savapage.core.util.DateUtil;
import org.savapage.ext.ServerPluginContext;
import org.savapage.ext.payment.PaymentGatewayException;
import org.savapage.ext.payment.PaymentGatewayTrx;
import org.savapage.ext.payment.PaymentGatewayTrxEvent;
import org.savapage.ext.payment.PaymentMethodEnum;
import org.savapage.ext.payment.bitcoin.IBitcoinGateway;
import org.savapage.ext.payment.bitcoin.IBitcoinGatewayListener;
import org.savapage.ext.payment.bitcoin.BitcoinGatewayTrx;
import org.savapage.ext.payment.bitcoin.BitcoinWalletInfo;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.AddressList;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.ExchangeRate;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.NewAddress;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class BlockchainInfoPlugin implements IBitcoinGateway {

    /**
     * The URL to send payment requests to.
     */
    private static final String PROP_KEY_RECEIVE_API_URL = "receive.api.url";

    /**
     * The URL to get the exchange rates.
     */
    private static final String PROP_KEY_TICKER_API_URL = "ticker.api.url";

    /**
     * A comma separated value list of ISO currency codes for which exchanges
     * rates are available.
     */
    private static final String PROP_KEY_TICKER_CURRENCY_CODES =
            "ticker.currency-codes";

    /**
     * The base URL of the Wallet API.
     */
    private static final String PROP_KEY_WALLET_API_URL = "wallet.api.url";

    /**
     * .
     */
    private static final String PROP_KEY_WALLET_IDENTIFIER =
            "wallet.identifier";

    /**
     * .
     */
    private static final String PROP_KEY_WALLET_PASSWORD_MAIN =
            "wallet.password.main";

    /**
     * .
     */
    private static final String PROP_KEY_WALLET_PASSWORD_SECOND =
            "wallet.password.second";

    /**
     * .
     */
    private static final String PROP_KEY_WALLET_URL = "wallet.url";

    /**
     * .
     */
    private static final String PROP_KEY_TRUST_CONFIRMATIONS_ACK =
            "trust.confirmations.acknowledge";

    /**
     * .
     */
    private static final String PROP_KEY_TRUST_CONFIRMATIONS_TRUST =
            "trust.confirmations.trust";

    /**
     * .
     */
    private static final String PROP_KEY_CALLBACK_SECRET_PARM =
            "callback.secret.parm";
    /**
     * .
     */
    private static final String PROP_KEY_CALLBACK_SECRET_VALUE =
            "callback.secret.value";

    /**
     * Timeout in milliseconds until a HTTP connection with server is
     * established.
     */
    private static final String PROP_KEY_API_TIMEOUT_CONNECT_MILLIS =
            "api.timeout.connect";

    /** */
    private static final String PROP_VALUE_API_TIMEOUT_CONNECT_MILLIS = "3000";

    /**
     * Timeout in milliseconds to receive data on established connection with
     * server, i.e. maximum time of inactivity between two data packets.
     */
    private static final String PROP_KEY_API_TIMEOUT_SOCKET_MILLIS =
            "api.timeout.socket";

    /** */
    private static final String PROP_VALUE_API_TIMEOUT_SOCKET_MILLIS = "3000";

    /**
     * Number of days after which bitcoin addresses, that have not received
     * transactions, will be consolidated.
     */
    private static final String PROP_KEY_ADDRESS_CONSOLIDATION_DAYS =
            "bitcoin.address.consolidation.days";

    /** */
    private static final String PROP_VALUE_ADDRESS_CONSOLIDATION_DAYS = "60";

    /**
     * The cycle in hours to perform the consolidate action.
     */
    private static final String PROP_KEY_ADDRESS_CONSOLIDATION_CYCLE =
            "bitcoin.address.consolidation.cycle.hours";

    /** */
    private static final String PROP_VALUE_ADDRESS_CONSOLIDATION_CYCLE = "48";

    /**
     * The number of confirmations of this transaction.
     */
    private static final String CALLBACK_KEY_CONFIRMATIONS = "confirmations";

    /**
     * The value of the payment received in satoshi. Divide by 100000000 to get
     * the value in BTC.
     */
    private static final String CALLBACK_KEY_VALUE = "value";

    /**
     * The bitcoin address to receive the transaction.
     */
    private static final String CALLBACK_KEY_INPUT_ADDRESS = "input_address";

    /**
     * The transaction hash. String x(64).
     */
    private static final String CALLBACK_KEY_TRANSACTION_HASH =
            "transaction_hash";

    /**
     * Signal that the callback was processed.
     */
    private static final String CALLBACK_RESPONSE_OK = "*ok*";

    /**
     *
     */
    private static final BigDecimal MIN_BTC = BigDecimal.valueOf(0.0005);

    /**
     * .
     */
    private IBitcoinGatewayListener listener;

    /**
     * .
     */
    private Properties properties;

    /**
     *
     */
    private String id;

    /**
     *
     */
    private String name;

    /**
     * Timeout in milliseconds until a HTTP connection with server is
     * established.
     */
    private int connectTimeout;

    /**
     * Timeout in milliseconds to receive data on established connection with
     * server, i.e. maximum time of inactivity between two data packets.
     */
    private int socketTimeout;

    /**
     * Test mode is not applicable for this plug-in. Use the
     * <a href="https://blockchain.info/api/api_receive">blockchain.info</a>
     * test page.
     */
    private final boolean live = true;

    /**
     *
     */
    private boolean online = true;

    /**
     * .
     */
    private int trustConfirmations = 1;

    /**
     * .
     */
    private int ackConfirmations = 0;

    /**
     * .
     */
    private String callbackSecretParm;

    /**
     * .
     */
    private String callbackSecretValue;

    /**
     * Number of days after which bitcoin addresses, that have not received
     * transactions, will be consolidated.
     */
    private int consolidateDays;

    /**
     * The cycle in hours to perform the consolidate action.
     */
    private int consolidateCycleHours;

    /**
     * .
     */
    private long consolidateLastTime = 0;

    /**
     * The URL to the Wallet identifier or alias.
     */
    private URL walletUrl;

    /**
     *
     */
    private final AtomicInteger walletAddressCount = new AtomicInteger(0);

    /**
     *
     */
    private final AtomicInteger walletAddressCountOpen = new AtomicInteger(0);

    /**
     *
     */
    private final AtomicInteger walletAddressCountReceived =
            new AtomicInteger(0);

    /**
     *
     */
    private final Set<String> secretUrlParms = new HashSet<>();

    /**
     * ISO currency codes for which exchanges rates are available.
     */
    private final Set<String> exchangeCurrencyCodes = new HashSet<>();

    /**
     * Removes (consolidates) inactive archived addresses from the wallet, and
     * archives addresses dedicated to user payments of which the balance is
     * zero (0) and for which a payment is received.
     * <p>
     * Note: Blockchain.info wallet can hold a limited number of active
     * addresses (999 is the maximum). To prevent address "overflow", bitcoins
     * MUST be transferred to another wallet or an external (IBAN) account
     * manually before the limit is reached. This will cause (many) addresses to
     * have a balance of zero (0), so this archiving action makes sense.
     * </p>
     *
     * @param forceAutoConsolidate
     *            When {@code true} the Auto Consolidate action is always
     *            executes, when {@code false} it is executed when
     *            {@link #consolidateCycleHours} has passed after the last
     *            prune.
     * @param addressArchiving
     *            When {@code true} address archiving and count is executed.
     * @throws PaymentGatewayException
     *             When errors.
     */
    private synchronized void pruneBitcoinAddresses(
            final boolean forceAutoConsolidate, final boolean addressArchiving)
            throws PaymentGatewayException {

        final long now = System.currentTimeMillis();

        if (forceAutoConsolidate) {
            consolidateLastTime = 0;
        }

        final boolean consolidate =
                consolidateLastTime + this.consolidateCycleHours
                        * DateUtil.DURATION_MSEC_HOUR > now;

        if (!consolidate && !addressArchiving) {
            return;
        }

        final BlockchainInfoClient client = new BlockchainInfoClient(this);

        try {

            if (consolidate) {
                client.autoConsolidate(this.consolidateDays);
            }

            if (addressArchiving) {

                int addressCount = 0;
                int addressCountOpen = 0;
                int addressCountReceived = 0;

                for (final AddressList.Address addr : client
                        .listAddresses(this.trustConfirmations)
                        .getAddresses()) {

                    addressCount++;

                    if (addr.getLabel() == null) {
                        /*
                         * Address not managed by this plug-in.
                         */
                        continue;
                    }

                    if (addr.getBalance().longValue() != 0) {
                        /*
                         * Since this address has a balance GT zero we want it
                         * to be part of the Wallet, and we do NOT archive
                         * (since archiving an address removes it from the
                         * wallet).
                         */
                        addressCountReceived++;
                        continue;
                    }

                    if (addr.getTotalReceived().longValue() == 0) {
                        /*
                         * Address is dedicated for a user, but no payment is
                         * received yet.
                         */
                        addressCountOpen++;
                        continue;
                    }

                    /*
                     * User has paid to this address, since the balance is zero
                     * and does not add up to the wallet balance, we can remove
                     * it from the wallet by archiving it.
                     */
                    client.archiveAddress(addr.getAddress());
                }

                this.walletAddressCount.set(addressCount);
                this.walletAddressCountOpen.set(addressCountOpen);
                this.walletAddressCountReceived.set(addressCountReceived);
            }

        } catch (IOException e) {
            this.listener.onPluginException(this, e);
        }

        if (consolidate) {
            consolidateLastTime = now;
        }
    }

    @Override
    public void onStart() throws PaymentGatewayException {
        pruneBitcoinAddresses(true, true);
    }

    @Override
    public void onStop() {
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isLive() {
        return this.live;
    }

    @Override
    public boolean isCurrencySupported(String currencyCode) {
        return this.exchangeCurrencyCodes.contains(currencyCode);
    }

    @Override
    public PaymentGatewayTrx onPaymentRequest(final PaymentRequest req)
            throws IOException, PaymentGatewayException {

        /**
         * INVARIANT: currency code must be supported.
         */
        if (!this.isCurrencySupported(req.getCurrency().getCurrencyCode())) {
            throw new PaymentGatewayException(
                    String.format("Currency code %s is not supported.",
                            req.getCurrency().getCurrencyCode()));
        }

        /*
         * INVARIANT: The minimum transaction size is 0.0005 BTC (approx. €0.10)
         * . There is no maximum payment or any other limitations.
         */

        // TODO

        /*
         * INVARIANT: user id must be specified.
         */
        if (StringUtils.isBlank(req.getUserId())) {
            throw new PaymentGatewayException("Userid not specified.");
        }

        final NewAddress recAddress =
                new BlockchainInfoClient(this).newAddress(req.getUserId());

        final PaymentGatewayTrx dto = new PaymentGatewayTrx();

        dto.setUserId(req.getUserId());
        dto.setAmount(BigDecimal.valueOf(req.getAmount()));
        dto.setTransactionId(recAddress.getAddress());
        dto.setPaymentMethod(PaymentMethodEnum.BITCOIN);

        return dto;
    }

    @Override
    public void onInit(final String id, final String name, final boolean live,
            final boolean online, final Properties props,
            final ServerPluginContext context) {

        this.id = id;
        this.name = name;
        this.properties = props;
        this.online = online;

        this.callbackSecretParm =
                props.getProperty(PROP_KEY_CALLBACK_SECRET_PARM);

        this.callbackSecretValue =
                props.getProperty(PROP_KEY_CALLBACK_SECRET_VALUE);

        this.ackConfirmations = Integer.valueOf(
                props.getProperty(PROP_KEY_TRUST_CONFIRMATIONS_ACK, "0"))
                .intValue();

        this.trustConfirmations = Integer.valueOf(
                props.getProperty(PROP_KEY_TRUST_CONFIRMATIONS_TRUST, "1"))
                .intValue();

        this.connectTimeout = Integer
                .valueOf(props.getProperty(PROP_KEY_API_TIMEOUT_CONNECT_MILLIS,
                        PROP_VALUE_API_TIMEOUT_CONNECT_MILLIS))
                .intValue();

        this.socketTimeout = Integer
                .valueOf(props.getProperty(PROP_KEY_API_TIMEOUT_SOCKET_MILLIS,
                        PROP_VALUE_API_TIMEOUT_SOCKET_MILLIS))
                .intValue();

        this.consolidateDays = Integer
                .valueOf(props.getProperty(PROP_KEY_ADDRESS_CONSOLIDATION_DAYS,
                        PROP_VALUE_ADDRESS_CONSOLIDATION_DAYS))
                .intValue();

        this.consolidateCycleHours = Integer
                .valueOf(props.getProperty(PROP_KEY_ADDRESS_CONSOLIDATION_CYCLE,
                        PROP_VALUE_ADDRESS_CONSOLIDATION_CYCLE))
                .intValue();

        this.secretUrlParms.add(this.callbackSecretParm);

        //
        final String currencyCodes =
                props.getProperty(PROP_KEY_TICKER_CURRENCY_CODES, "");

        for (final String currencyCode : StringUtils.split(currencyCodes,
                " ,;:")) {
            try {
                this.exchangeCurrencyCodes.add(Currency
                        .getInstance(currencyCode.trim()).getCurrencyCode());
            } catch (Exception e) {
                // noop
            }
        }

        this.walletUrl = null;

        if (props.containsKey(PROP_KEY_WALLET_URL)) {
            try {
                this.walletUrl =
                        new URL(props.getProperty(PROP_KEY_WALLET_URL));
            } catch (MalformedURLException e) {
                // noop
            }
        }

    }

    @Override
    public void onInit(final IBitcoinGatewayListener listener) {
        this.listener = listener;
    }

    /**
     *
     * @param parameterMap
     * @param key
     * @param dfault
     * @return
     */
    private long getLong(final Map<String, String[]> parameterMap,
            final String key, final long dfault) {
        return Long
                .parseLong(getString(parameterMap, key, Long.toString(dfault)));
    }

    /**
     *
     * @param parameterMap
     * @param key
     * @return
     */
    private String getString(final Map<String, String[]> parameterMap,
            final String key) {
        return getString(parameterMap, key, null);
    }

    /**
     *
     * @param parameterMap
     * @param key
     * @param dfault
     * @return
     */
    private String getString(final Map<String, String[]> parameterMap,
            final String key, final String dfault) {

        if (!parameterMap.containsKey(key)) {
            return dfault;
        }
        final String[] values = parameterMap.get(key);

        if (values == null || values.length != 1) {
            return dfault;
        }
        return values[0];
    }

    @Override
    public BitcoinWalletInfo getWalletInfo(Currency currency, boolean cached)
            throws PaymentGatewayException, IOException {

        pruneBitcoinAddresses(false, !cached);

        final BitcoinWalletInfo wallet = new BitcoinWalletInfo();

        wallet.setSatoshiBalance(
                new BlockchainInfoClient(this).getWalletBalance());
        wallet.setBitcoinExchangeRate(this.getExchangeRate(currency).getBuy());
        wallet.setCurrencyCode(currency.getCurrencyCode());
        wallet.setDate(new Date());

        wallet.setAddressCount(Integer.valueOf(this.walletAddressCount.get()));
        wallet.setAddressCountOpen(
                Integer.valueOf(this.walletAddressCountOpen.get()));
        wallet.setAddressCountReceived(
                Integer.valueOf(this.walletAddressCountReceived.get()));

        wallet.setWebPageUrl(this.walletUrl);

        return wallet;
    }

    @Override
    public CallbackResponse onCallBack(final Map<String, String[]> parameterMap,
            final boolean live, final Currency currency,
            final BufferedReader request, final PrintWriter response)
            throws IOException, PaymentGatewayException {

        BlockchainInfoLogger.logCallback(parameterMap, this.secretUrlParms);

        /*
         * INVARIANT: currency code must be supported.
         */
        if (!this.isCurrencySupported(currency.getCurrencyCode())) {
            throw new PaymentGatewayException(
                    String.format("Currency code %s is not supported.",
                            currency.getCurrencyCode()));
        }

        /*
         * INVARIANT: secret must match.
         */
        if (!getString(parameterMap, this.callbackSecretParm, "")
                .equals(this.callbackSecretValue)) {
            throw new PaymentGatewayException(
                    String.format("%s mismatch.", this.callbackSecretParm));
        }

        /*
         * INVARIANT: value must be present.
         */
        final long satoshiValue = getLong(parameterMap, CALLBACK_KEY_VALUE, 0L);

        if (satoshiValue == 0L) {
            throw new PaymentGatewayException(
                    String.format("%s missing.", CALLBACK_KEY_VALUE));
        }

        /*
         * Acknowledge the transaction at zero confirmations but only trust the
         * transaction after more confirmations.
         */
        final Long confirmations =
                getLong(parameterMap, CALLBACK_KEY_CONFIRMATIONS, 0L);

        //
        //
        final CallbackResponse callbackResponse = new CallbackResponse(
                HttpStatus.SC_OK, "text/plain; charset=us-ascii");

        /*
         * Always trust transactions with a negative satoshi value. These are
         * payments made FROM the Wallet.
         */
        final boolean isTrusted = (confirmations >= this.trustConfirmations)
                || (satoshiValue < 0L);

        /*
         * WARNING: "Callback domains which appear dead or never return the
         * "*ok*" response may be blocked from the service."
         */
        if (isTrusted) {
            /*
             * IMPORTANT: write callback response FIRST, to make sure that in
             * case a PaymentGatewayException occurs (e.g. because user is not
             * found) the response is send back to the Blockchain.info server.
             */
            response.write(CALLBACK_RESPONSE_OK);
        }

        if (satoshiValue > 0L) {

            final boolean isAcknowledged =
                    confirmations >= this.ackConfirmations;

            /*
             * IMPORTANT: A payment FROM the Wallet can result in a transaction
             * with a positive satoshi value.
             */

            /*
             * INVARIANT: A notification will never be sent for the same
             * transaction twice once acknowledged with "*ok*". It is a good
             * idea to record the transaction hash even if not needed to detect
             * duplicates for logging purposes.
             */

            /*
             * Create an independent DTO and pass to the listener.
             */
            final BitcoinGatewayTrx trx = new BitcoinGatewayTrx();

            trx.setGatewayId(this.getId());

            trx.setTransactionId(
                    getString(parameterMap, CALLBACK_KEY_TRANSACTION_HASH));

            trx.setTransactionAddress(
                    getString(parameterMap, CALLBACK_KEY_INPUT_ADDRESS));

            trx.setConfirmations(confirmations.intValue());

            trx.setSatoshi(satoshiValue);

            trx.setExchangeCurrencyCode(currency.getCurrencyCode());
            trx.setExchangeRate(this.getExchangeRate(currency).getBuy());

            trx.setAcknowledged(isAcknowledged);
            trx.setTrusted(isTrusted);

            trx.setUserId(null);

            //
            final PaymentGatewayTrxEvent trxEvent =
                    this.listener.onPaymentConfirmed(trx);

            callbackResponse.setPluginObject(trxEvent);
        }

        return callbackResponse;
    }

    @Override
    public void onCallBackCommitted(Object pluginObject)
            throws PaymentGatewayException {

        if (pluginObject != null) {
            final PaymentGatewayTrxEvent event =
                    (PaymentGatewayTrxEvent) pluginObject;
            this.listener.onPaymentCommitted(event);
        }

        pruneBitcoinAddresses(false, true);
    }

    /**
     * Gets the exchange rate for a {@link Currency}.
     *
     * @param currency
     *            The {@link Currency}.
     * @return The {@link ExchangeRate}.
     * @throws PaymentGatewayException
     *             When not found.
     * @throws IOException
     */
    private ExchangeRate getExchangeRate(final Currency currency)
            throws PaymentGatewayException, IOException {

        final ExchangeRate exchange =
                new BlockchainInfoClient(this).getExchangeRate(currency);

        if (exchange == null) {
            throw new PaymentGatewayException(
                    String.format("No BTC exchange rate found for %s",
                            currency.getCurrencyCode()));
        }

        return exchange;
    }

    /**
     *
     * @return
     */
    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_RECEIVE_API_URL}.
     */
    public String getReceiveApiUrl() {
        return this.properties.getProperty(PROP_KEY_RECEIVE_API_URL);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_TICKER_API_URL}.
     */
    public String getTickerApiUrl() {
        return this.properties.getProperty(PROP_KEY_TICKER_API_URL);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_WALLET_API_URL}.
     */
    public String getWalletApiUrl() {
        return this.properties.getProperty(PROP_KEY_WALLET_API_URL);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_WALLET_IDENTIFIER}.
     */
    public String getWalletId() {
        return this.properties.getProperty(PROP_KEY_WALLET_IDENTIFIER);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_WALLET_PASSWORD_MAIN}.
     */
    public String getWalletPasswordMain() {
        return this.properties.getProperty(PROP_KEY_WALLET_PASSWORD_MAIN);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_WALLET_PASSWORD_MAIN}, or
     *         {@code null} when not present.
     */
    public String getWalletPasswordSecond() {
        return this.properties.getProperty(PROP_KEY_WALLET_PASSWORD_SECOND);
    }

    @Override
    public BigDecimal getMinimumBtcPayment() {
        return MIN_BTC;
    }

    @Override
    public boolean isOnline() {
        return this.online;
    }

    @Override
    public void setOnline(boolean online) {
        this.online = online;
    }

}
