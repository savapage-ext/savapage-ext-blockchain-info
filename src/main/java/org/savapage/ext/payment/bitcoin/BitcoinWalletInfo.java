/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.bitcoin;

import java.net.URL;
import java.util.Date;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class BitcoinWalletInfo implements Cloneable {

    private long satoshiBalance;

    private String currencyCode;

    private double bitcoinExchangeRate;

    private Date date;

    private Integer addressCount;
    private Integer addressCountOpen;
    private Integer addressCountReceived;

    /**
     * The URL to the Wallet Web Page (can be {@code null}).
     */
    private URL webPageUrl;

    /**
     *
     * @return The Wallet balance in satoshi units.
     */
    public long getSatoshiBalance() {
        return satoshiBalance;
    }

    /**
     *
     * @param satoshiBalance
     *            The Wallet balance in satoshi units.
     */
    public void setSatoshiBalance(long satoshiBalance) {
        this.satoshiBalance = satoshiBalance;
    }

    /**
     *
     * @return The ISO currency code. E.g. "EUR", "USD", ...
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     *
     * @param currencyCode
     *            The ISO currency code. E.g. "EUR", "USD", ...
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     *
     * @return The exchange rate from BTC to {@link #currencyCode}.
     */
    public double getBitcoinExchangeRate() {
        return bitcoinExchangeRate;
    }

    /**
     *
     * @param bitcoinExchangeRate
     *            The exchange rate from BTC to {@link #currencyCode}.
     */
    public void setBitcoinExchangeRate(double bitcoinExchangeRate) {
        this.bitcoinExchangeRate = bitcoinExchangeRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return The total number of Bitcoin addresses as present in the Wallet or
     *         {@code null} if unknown.
     */
    public Integer getAddressCount() {
        return addressCount;
    }

    /**
     * Sets the total number of Bitcoin addresses in the Wallet.
     *
     * @param addressCount
     *            Number of addresses or {@code null} if unknown.
     */
    public void setAddressCount(Integer addressCount) {
        this.addressCount = addressCount;
    }

    /**
     * @return The number of Bitcoin addresses in the Wallet that have
     *         <i>not</i> received a payment or {@code null} if unknown.
     */
    public Integer getAddressCountOpen() {
        return addressCountOpen;
    }

    /**
     * Sets the number of Bitcoin addresses in the Wallet that have <i>not</i>
     * received a payment.
     *
     * @param addressCountOpen
     *            Number of addresses or {@code null} if unknown.
     */
    public void setAddressCountOpen(Integer addressCountOpen) {
        this.addressCountOpen = addressCountOpen;
    }

    /**
     *
     * @return The number of Bitcoin addresses in the Wallet that <i>have</i>
     *         received a payment or {@code null} if unknown.
     */
    public Integer getAddressCountReceived() {
        return addressCountReceived;
    }

    /**
     * Gets the number of Bitcoin addresses in the Wallet that <i>have</i>
     * received a payment.
     *
     * @param addressCountReceived
     *            Number of addresses or {@code null} if unknown.
     */
    public void setAddressCountReceived(Integer addressCountReceived) {
        this.addressCountReceived = addressCountReceived;
    }

    /**
     * @return The URL to the Wallet Web Page ({@code null} when not available).
     */
    public URL getWebPageUrl() {
        return webPageUrl;
    }

    /**
     * @param webPageUrl
     *            The URL to the Wallet Web Page ({@code null} when not
     *            available).
     */
    public void setWebPageUrl(URL webPageUrl) {
        this.webPageUrl = webPageUrl;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        final BitcoinWalletInfo info = (BitcoinWalletInfo) super.clone();
        info.date = (Date) this.date.clone();
        return info;
    }
}
