/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2015 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2015 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.bitcoin.blockchaininfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Currency;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.savapage.core.util.IOHelper;
import org.savapage.ext.payment.PaymentGatewayException;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.AddressList;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.ArchivedAddress;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.ExchangeRate;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.NewAddress;
import org.savapage.ext.payment.bitcoin.blockchaininfo.json.WalletBalance;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class BlockchainInfoClient {

    /**
     * .
     */
    private final BlockchainInfoPlugin paymentPlugin;

    /**
     *
     * @param plugin
     *            The {@link BlockchainInfoPlugin}.
     */
    public BlockchainInfoClient(final BlockchainInfoPlugin plugin) {
        this.paymentPlugin = plugin;
    }

    /**
     * Gets the exchanges rates.
     *
     * @param currency
     *            The {@link Currency}.
     * @return The {@link ExchangeRate}.
     * @throws PaymentGatewayException
     * @throws IOException
     *             When communication errors.
     */
    public ExchangeRate getExchangeRate(final Currency currency)
            throws PaymentGatewayException, IOException {

        final URI uri = this.getTickerApiUri();

        final HttpGet httpRequest = new HttpGet(uri);
        final StringBuilder responseBuilder = new StringBuilder();

        final int httpStatus = this.send(httpRequest, responseBuilder, false);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {

            final ExchangeRate rate = ExchangeRate.createExchangeRates(jsonOut)
                    .get(currency.getCurrencyCode());

            BlockchainInfoLogger.logResponse(String.format("{ \"%s\" : %s }",
                    currency.getCurrencyCode(), rate.stringifyPrettyPrinted()),
                    httpStatus);

            return rate;

        } else {
            throw new PaymentGatewayException(
                    String.format("%s: %s", "Get ExchangeRate", jsonOut));
        }
    }

    /**
     * Gets the balance of the Blockchain.info wallet.
     *
     * @return The balance in satochi units.
     * @throws PaymentGatewayException
     *             When logical errors.
     * @throws IOException
     *             When communication errors.
     */
    public long getWalletBalance() throws PaymentGatewayException, IOException {

        final StringBuilder builder = this.buildWalletApiUri("balance", false);

        final URI uri = this.composeWalletApiUri(builder);

        final HttpGet httpRequest = new HttpGet(uri);
        final StringBuilder responseBuilder = new StringBuilder();

        final int httpStatus = this.send(httpRequest, responseBuilder, true);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {
            return WalletBalance.create(jsonOut).getBalance();
        } else {
            throw new PaymentGatewayException(
                    String.format("%s: %s", "Get Wallet Balance", jsonOut));
        }
    }

    /**
     * Archives a bitcoin address.
     * <p>
     * To improve wallet performance addresses which have not been used recently
     * should be moved to an archived state. They will still be held in the
     * wallet but will no longer be included in the "list" or
     * "list-transactions" calls.
     * </p>
     * <p>
     * If a payment from a user is acknowledged the address should be archived.
     * </p>
     *
     * @param address
     *            The address.
     * @throws IOException
     *             When communication errors.
     * @throws PaymentGatewayException
     */
    public void archiveAddress(final String address)
            throws IOException, PaymentGatewayException {

        final StringBuilder builder =
                this.buildWalletApiUri("archive_address", true)
                        .append("&address=").append(address);

        final URI uri = this.composeWalletApiUri(builder);

        final HttpGet httpRequest = new HttpGet(uri);

        final StringBuilder responseBuilder = new StringBuilder();

        final ArchivedAddress archivedAddress;

        final int httpStatus = this.send(httpRequest, responseBuilder, true);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {

            archivedAddress = ArchivedAddress.create(jsonOut);

            if (archivedAddress.getError() != null) {
                throw new PaymentGatewayException(String.format(
                        "Error archiving Bitcoin address [%s] from [%s]: %s",
                        address, this.paymentPlugin.getId(),
                        archivedAddress.getError()));
            }

        } else {
            throw new PaymentGatewayException(String.format(
                    "Error getting new Bitcoin address [%s] from [%s]", address,
                    this.paymentPlugin.getId()));
        }

    }

    /**
     * Generates a new address.
     *
     * @param label
     *            The label of the new address.
     * @return The {@link BlockchainRecAddress}.
     * @throws IOException
     *             When communication errors.
     * @throws PaymentGatewayException
     *             When HTTP response status NEQ "201 Created".
     */
    public NewAddress newAddress(final String label)
            throws IOException, PaymentGatewayException {

        final StringBuilder builder =
                this.buildWalletApiUri("new_address", true);

        builder.append("&label=").append(URLEncoder.encode(label, "UTF-8"));

        final URI uri = this.composeWalletApiUri(builder);

        final HttpGet httpRequest = new HttpGet(uri);

        final StringBuilder responseBuilder = new StringBuilder();

        final NewAddress recAddress;

        final int httpStatus = this.send(httpRequest, responseBuilder, true);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {

            recAddress = NewAddress.create(jsonOut);

            if (recAddress.getError() != null) {
                throw new PaymentGatewayException(String.format(
                        "Error getting new Bitcoin address from [%s]: %s",
                        this.paymentPlugin.getId(), recAddress.getError()));
            }

        } else {
            throw new PaymentGatewayException(
                    String.format("Error getting new Bitcoin address from [%s]",
                            this.paymentPlugin.getId()));
        }
        return recAddress;
    }

    /**
     * Removes some inactive archived addresses from the wallet and insert them
     * as forwarding addresses (see receive payments API).
     * <p>
     * You will still receive callback notifications for these addresses however
     * they will no longer be part of the main wallet and will be stored server
     * side.
     * </p>
     * <p>
     * If generating a lot of addresses it is a recommended to call this method
     * at least every 48 hours. A good value for days is 60 i.e. addresses which
     * have not received transactions in the last 60 days will be consolidated.
     * </p>
     *
     * @param days
     *            Number of days after which bitcoin addresses, that have not
     *            received transactions, will be consolidated.
     * @throws IOException
     *             When communication errors.
     * @throws PaymentGatewayException
     */
    public BlockchainInfoConsolidatedAddresses autoConsolidate(final int days)
            throws IOException, PaymentGatewayException {

        final StringBuilder builder =
                this.buildWalletApiUri("auto_consolidate", true)
                        .append("&days=").append(days);

        final URI uri = this.composeWalletApiUri(builder);

        final HttpGet httpRequest = new HttpGet(uri);

        final StringBuilder responseBuilder = new StringBuilder();

        final BlockchainInfoConsolidatedAddresses consolidatesAddresses;

        final int httpStatus = this.send(httpRequest, responseBuilder, true);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {

            consolidatesAddresses =
                    BlockchainInfoConsolidatedAddresses.create(jsonOut);

            if (consolidatesAddresses.getError() != null) {
                throw new PaymentGatewayException(String.format(
                        "Error consolidating Bitcoin addresses from [%s]: %s",
                        this.paymentPlugin.getId(),
                        consolidatesAddresses.getError()));
            }

        } else {
            throw new PaymentGatewayException(String.format(
                    "Error consolidating Bitcoin addresses from [%s]",
                    this.paymentPlugin.getId()));
        }
        return consolidatesAddresses;
    }

    /**
     * @param confirmations
     *            The minimum number of confirmations transactions must have
     *            before being included in balance of addresses.
     * @return The {@link AddressList}.
     * @throws IOException
     * @throws PaymentGatewayException
     */
    public AddressList listAddresses(int confirmations)
            throws IOException, PaymentGatewayException {

        final StringBuilder builder = this.buildWalletApiUri("list", false)
                .append("&confirmations=").append(confirmations);

        final URI uri = this.composeWalletApiUri(builder);

        final HttpGet httpRequest = new HttpGet(uri);

        final StringBuilder responseBuilder = new StringBuilder();

        final AddressList addressList;

        final int httpStatus = this.send(httpRequest, responseBuilder, true);

        final String jsonOut = responseBuilder.toString();

        if (httpStatus == HttpStatus.SC_OK) {

            addressList = AddressList.create(jsonOut);

            if (addressList.getError() != null) {
                throw new PaymentGatewayException(String.format(
                        "Error retrieving Bitcoin address list from [%s]: %s",
                        this.paymentPlugin.getId(), addressList.getError()));
            }

        } else {
            throw new PaymentGatewayException(String.format(
                    "Error retrieving Bitcoin address list from [%s]",
                    this.paymentPlugin.getId()));
        }
        return addressList;
    }

    /**
     * Creates a {@link RequestConfig} with the configured connect and socket
     * timeout values.
     *
     * @return The {@link RequestConfig}.
     */
    private RequestConfig buildRequestConfig() {

        return RequestConfig.custom()
                .setConnectTimeout(this.paymentPlugin.getConnectTimeout())
                .setSocketTimeout(this.paymentPlugin.getSocketTimeout())
                .setConnectionRequestTimeout(
                        this.paymentPlugin.getSocketTimeout())
                .build();
    }

    /**
     * Sends an HTTP request, collects the response and returns the HTTP status
     * code.
     *
     * @param httpRequest
     *            The {@link HttpRequestBase}.
     * @param jsonOut
     *            The {@link StringBuilder} to append the JSON response to.
     * @return The HTTP status code.
     * @throws IOException
     *             When communication errors.
     */
    private int send(final HttpRequestBase httpRequest,
            final StringBuilder jsonOut, final boolean logResponse)
            throws IOException {

        final URI uri = httpRequest.getURI();

        BlockchainInfoLogger.logRequest(StringUtils.replace(uri.getPath(),
                this.paymentPlugin.getWalletId(), "..."));

        //
        final CloseableHttpClient httpClient = HttpClients.createMinimal();

        // Set timeout values
        httpRequest.setConfig(buildRequestConfig());

        final int statusCode;

        ByteArrayOutputStream bos;
        CloseableHttpResponse response = null;

        try {
            // Send it ...
            response = httpClient.execute(httpRequest);

            statusCode = response.getStatusLine().getStatusCode();

            bos = new ByteArrayOutputStream();
            response.getEntity().writeTo(bos);

            jsonOut.append(bos.toString());

            if (logResponse) {
                BlockchainInfoLogger.logResponse(jsonOut.toString(),
                        statusCode);
            }

        } catch (IOException ex) {

            BlockchainInfoLogger.logException(ex);
            throw ex;

        } finally {
            /*
             * Mantis #487: release the connection.
             */
            httpRequest.reset();
            //
            IOHelper.closeQuietly(response);
            IOHelper.closeQuietly(httpClient);
        }

        return statusCode;
    }

    /**
     * Gets the Ticker {@link URI}.
     *
     * @return The {@link URI}.
     */
    private URI getTickerApiUri() {
        try {
            return new URI(this.paymentPlugin.getTickerApiUrl());
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param command
     * @return
     */
    private StringBuilder buildWalletApiUri(final String command,
            final boolean isSecondPasswordNeeded) {

        final StringBuilder builder = new StringBuilder();

        try {
            builder.append(this.paymentPlugin.getWalletApiUrl()).append("/")
                    .append(this.paymentPlugin.getWalletId()).append("/")
                    .append(command).append("?password=")
                    .append(URLEncoder.encode(
                            this.paymentPlugin.getWalletPasswordMain(),
                            "UTF-8"));

            if (isSecondPasswordNeeded) {

                final String secondPw =
                        this.paymentPlugin.getWalletPasswordSecond();

                if (StringUtils.isNotBlank(secondPw)) {
                    builder.append("&second_password=")
                            .append(URLEncoder.encode(
                                    this.paymentPlugin
                                            .getWalletPasswordSecond(),
                                    "UTF-8"));
                }
            }

        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }

        return builder;
    }

    private URI composeWalletApiUri(final StringBuilder builder) {
        try {
            return new URI(builder.toString());
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

}
