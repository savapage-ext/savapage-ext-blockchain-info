<!-- 
    SPDX-FileCopyrightText: (c) 2015 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

# savapage-ext-blockchain-info
    
Payment Gateway Plug-in for Bitcoins using [Blockchain.info](https://blockchain.info) Wallet and API.

 
### License

This module is part of the SavaPage project <https://www.savapage.org>,
copyright (c) 2015 Datraverse B.V. and licensed under the
[GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl.html)
version 3, or (at your option) any later version.

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Members. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Usage

Copy plug-in to server environment:

    $ sudo cp savapage-ext-blockchain-info.properties /opt/savapage/server/ext
    $ sudo target/savapage-ext-blockchain.jar /opt/savapage/server/ext/lib

Set ownership and restrict permissions, since properties file contains confidential information:
    
    $ sudo chown savapage:savapage /opt/savapage/server/ext/savapage-ext-blockchain-info.properties
    $ sudo chmod 600 /opt/savapage/server/ext/savapage-ext-blockchain-info.properties

Edit `savapage-ext-blockchain-info.properties` to specify your wallet and callback parameters.
    
Edit `/opt/savapage/server/lib/log4j.properties` and add:  
    
    #------------------------------------------------------------------------------
    # Blockchain.info Payment Gateway Plugin
    #------------------------------------------------------------------------------
    log4j.appender.ext_blockchain_info=org.apache.log4j.RollingFileAppender
    log4j.appender.ext_blockchain_info.MaxFileSize=10MB
    log4j.appender.ext_blockchain_info.MaxBackupIndex=10
    log4j.appender.ext_blockchain_info.File=${server.home}/ext/logs/blockchain-info.log
    log4j.appender.ext_blockchain_info.layout=org.apache.log4j.PatternLayout
    log4j.appender.ext_blockchain_info.layout.ConversionPattern=%d{ISO8601} %m\n
    log4j.appender.ext_blockchain_info.encoding=UTF8
    
    # Use INFO to activate the BlockchainInfoLogger
    log4j.logger.org.savapage.ext.payment.bitcoin.blockchaininfo.BlockchainInfoLogger=TRACE, ext_blockchain_info
    log4j.additivity.org.savapage.ext.payment.bitcoin.blockchaininfo.BlockchainInfoLogger=false

Make sure `/opt/savapage/server/lib/log4j.properties` contains a logger for the  Payment Gateway Audit Trail as explained in the `README.md` of the `savapage-ext` project.

Restart SavaPage.

#### Limitations

*The Blockchain.info wallet can hold a limited number of active addresses (999 is the maximum).* 

To prevent adress "overflow" you are advised to manually transfer some of your wallet balance to another wallet or an external (IBAN) account regulary, at least before the limit is reached. This action will cause (many) receiving addresses to have a balance of zero (0): the plug-in automatically archives these addresses for you so the number of active addresses will be reduced.
